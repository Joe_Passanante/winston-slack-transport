/*
Author: Alexandra Martin
DevOps Project
29 March 2020

Simple Winston unit test for application
 */
describe('Winston Logger', () => {
  it('Created a logger', () => {
    const winston = require('winston');

    const loggerFormat = winston.format.printf(({ level, message }) => {
      return `${level}: ${message}`;
    });
    const logger = winston.createLogger({
      level: 'info',
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.padLevels(),
        loggerFormat
      ),

      transports: [new winston.transports.Console()],
    });

    logger.info('This is a test for Project Lumberjack');
    logger.info('A simple logger unit test.');
    logger.info('To aid in our Slack Transport creation.');
    logger.warn('TIMBER! THIS IS ONLY A TEST!');
    logger.info('Module 9');
    logger.error('The logs are getting away!');
  });
});
