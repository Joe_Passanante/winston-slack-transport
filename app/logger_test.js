const transport = require('./index');
const winston = require('winston');

const loggerFormat = winston.format.printf(({ level, message }) => {
  return `${level}: ${message}`;
});
const logger = winston.createLogger({
  level: 'info',
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.padLevels(),
        loggerFormat
      ),
    }),
    new transport({slackHook:'https://hooks.slack.com/services/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',timestamps:true}),
  ],
});
logger.info('Info 1...');
logger.info('Info 2...');
logger.info('Info 3...');
logger.warn('Warn 1...');
logger.info('Info...');
logger.error('ERROR!!!!!! AHHHHH');
