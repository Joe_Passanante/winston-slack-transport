const Transport = require('winston-transport')
const request = require('request-promise');

module.exports = class YourCustomTransport extends Transport {
  /**
   * @description Allows for logs to be sent to Slack using incoming webhooks. For more information on slack incoming webhooks, go to https://api.slack.com/messaging/webhooks
   * @param {Object} opts Options for Slack Transport
   * @param {String} opts.slackHook Webhook for slack
   * @param {Boolean} opts.timestamps Prepend timestamps to slack messages
   * @throws Missing slackHook from options - When no slackhook is defined in options
   * @example new SlackTransport({slackHook:'https://hooks.slack.com/services/XXXXX/XXXXX/XXXXX',timestamps:true})
   */
  constructor(opts) {
    super(opts);
    this.slackHook = opts.slackHook
    this.timestamps = opts.timestamps || false
  }
 
  log(info, callback) {
    setImmediate(() => {
      this.emit('logged', info);
    });
    if(!this.slackHook){
      throw new Error("Missing slackHook from options")
    }
    // Send the message to slack... async
    (async ()=>{
      try{
      const res = await request({
        url: this.slackHook,
        method: 'POST',
        body:  {'text':`${this.timestamps?new Date().toISOString():''} | *${info.level}*: ${info.message}`},
        json: true
      });
    }catch(err){
      //add error handling
    }
    })()
    callback();
  }
};