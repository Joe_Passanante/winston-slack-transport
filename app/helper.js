/*
Authors: Harrison Dominique & Reeda Hanif
DevOps Project
3 April 2020

Webhook - gets data and posts to slack
 */

const request = require('request-promise');

// const hook = '';


// gets data
const addLumberjack = async function(){

    const json = await request({
        // url:'',
        json: true
    });

    return json.map(slack => ({
            domain: "",
            apiToken:"",
            channel: "",
            username: "",
            level: 'error',
            handleExceptions: true
        })
    )};


(async function () {
    try {

        // logs will be entered in here sme variable tracking logs needed
        const payload = {
            text: `Hello World! Posted on: ${new Date().toLocaleTimeString()}`
        };

        // adds to Slack
        const res = await request({
            url:"https://hooks.slack.com/services/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",
            method: 'POST',
            body:  payload,
            json: true
        });

    }
    catch(e) {
        console.log('our error', e);
    }
})();
